package qrapp.school.mobile.mkotlarz.com.qrapp.utils;

import android.os.AsyncTask;
import android.text.Html;

import qrapp.school.mobile.mkotlarz.com.qrapp.interfaces.AsyncResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

/**
 * Created by Maciej Kotlarz on 2014-12-08.
 */
public class NetQuery extends AsyncTask<String, JSONObject, JSONObject  > implements AsyncResponse {

    public AsyncResponse Delegate = null;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        PreExecute();
    }

    @Override
    protected JSONObject doInBackground(String... params) {
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(params[0]);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            if(statusLine.getStatusCode() == HttpStatus.SC_OK) {
                HttpEntity entity = response.getEntity();
                String result =  Html.fromHtml(EntityUtils.toString(entity)).toString();
                JSONObject jsonObject = new JSONObject(result);
                return jsonObject;
            }
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);
        PostExecute(jsonObject);
    }

    @Override
    public void PreExecute() {
        if(Delegate != null)
            Delegate.PreExecute();
    }

    @Override
    public void PostExecute(JSONObject jsonObject) {
        if(Delegate != null)
           Delegate.PostExecute(jsonObject);
    }
}
