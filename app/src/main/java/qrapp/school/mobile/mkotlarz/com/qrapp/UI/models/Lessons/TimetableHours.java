package qrapp.school.mobile.mkotlarz.com.qrapp.UI.models.Lessons;

import java.util.ArrayList;

/**
 * Created by Maciej Kotlarz on 2014-12-10.
 */
public class TimetableHours {
    public String Start = null;
    public String End = null;
    private int m_hourId = 0;
    private ArrayList<Lesson> m_lessonsList = null;

    public TimetableHours(String Start, String End, int hourId) {
        this.Start = Start;
        this.End = End;
        m_hourId = hourId;
        m_lessonsList = new ArrayList<Lesson>();
        Initialize();
    }

    private void Initialize() {
        for (int i = 0; i < 7; i++) {
            m_lessonsList.add(new Lesson());
        }
    }

    public void AddLesson(Lesson lesson) {
        m_lessonsList.set(lesson.Day - 1, lesson);
    }
}
