package qrapp.school.mobile.mkotlarz.com.qrapp.UI.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import qrapp.school.mobile.mkotlarz.com.qrapp.UI.models.Lessons.Lesson;
import qrapp.school.mobile.mkotlarz.com.qrapp.UI.models.Lessons.TimetableHours;

/**
 * Created by Maciej Kotlarz on 2014-12-10.
 */
public class UIModelTimetable {

    private JSONObject m_jsonObject = null;
    private String m_schoolName = null;
    private String m_schoolToken = null;
    private ArrayList<TimetableHours> m_hours = null;

    public UIModelTimetable(JSONObject jsonObject) {
        m_jsonObject = jsonObject;
    }

    public void Proceed() {
        m_hours = new ArrayList<TimetableHours>();
        try {
            JSONObject data = m_jsonObject.getJSONObject("data");
            m_schoolName = data.getString("school_name");
            m_schoolToken = data.getString("school_token");
            JSONArray hours = data.getJSONArray("hours");
            for(int i = 0; i < hours.length(); i++) {
                JSONObject hourJSON = hours.getJSONObject(i);
                String start = hourJSON.getString("start");
                String end = hourJSON.getString("end");
                int hour_id = hourJSON.getInt("hour_id");
                TimetableHours hour = new TimetableHours(start, end, hour_id);
                JSONArray lessonsArray = hourJSON.getJSONArray("lessons");
                //Teraz nalezy iterowac przez godziny
                for(int j = 0; j < lessonsArray.length(); j++) {
                    JSONObject lessonJSON = lessonsArray.getJSONObject(j);
                    String subject = lessonJSON.getString("subject");
                    int subject_id = lessonJSON.getInt("subject_id");
                    String room = lessonJSON.getString("room");
                    int room_id = lessonJSON.getInt("room_id");
                    String teacher = lessonJSON.getString("teacher");
                    int teacher_id = lessonJSON.getInt("teacher_id");
                    int day = lessonJSON.getInt("day");
                    int lesson_id = lessonJSON.getInt("lesson_id");
                    Lesson lesson = new Lesson(subject, room, teacher, subject_id, room_id, teacher_id, day, lesson_id);
                    hour.AddLesson(lesson);
                }
                m_hours.add(hour);
            }
        } catch (Exception ex) {

        }
    }
}
