package qrapp.school.mobile.mkotlarz.com.qrapp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import qrapp.school.mobile.mkotlarz.com.qrapp.UI.models.SimpleSpinnerItem;

/**
 * Created by Maciej Kotlarz on 2014-12-08.
 */
public class TimetableJSONAdapter extends BaseAdapter implements SpinnerAdapter {

    private ArrayList<SimpleSpinnerItem> m_timetablesList = new ArrayList<SimpleSpinnerItem>();
    private JSONObject m_timetables = null;
    private Context m_context = null;
    private String m_contentTableName = null;
    private String m_nameColumnName = null;
    private String m_idColumnName = null;

    public TimetableJSONAdapter(JSONObject jsonObject, String nameColumnName, String idColumnName, String contentTableName, Context context) {
        m_timetables = jsonObject;
        m_context = context;
        m_contentTableName = contentTableName;
        m_idColumnName = idColumnName;
        m_nameColumnName = nameColumnName;
        parseData();
    }

    private void parseData() {
        try {
            JSONObject data = m_timetables.getJSONObject("data");
            JSONArray timetables = data.getJSONArray(m_contentTableName);
            for(int i = 0; i < timetables.length(); i++) {
                JSONObject timetable = timetables.getJSONObject(i);
                String name = timetable.getString(m_nameColumnName);
                int id = timetable.getInt(m_idColumnName);
                m_timetablesList.add(new SimpleSpinnerItem(name, id));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getCount() {
        return m_timetablesList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView text = new TextView(m_context);
        text.setTextColor(Color.BLACK);
        text.setText(m_timetablesList.get(position).getName());
        return text;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView text = new TextView(m_context);
        text.setTextColor(Color.BLACK);
        text.setText(m_timetablesList.get(position).getName());
        return text;
    }

    @Override
    public long getItemId(int position) {
        return m_timetablesList.get(position).getID();
    }

    @Override
    public Object getItem(int position) {
        return m_timetablesList.get(position-1);
    }
}
