package qrapp.school.mobile.mkotlarz.com.qrapp.interfaces;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Maciej Kotlarz on 2014-12-08.
 */
public interface AsyncResponse {
    void PreExecute();
    void PostExecute(JSONObject jsonObject);
}
