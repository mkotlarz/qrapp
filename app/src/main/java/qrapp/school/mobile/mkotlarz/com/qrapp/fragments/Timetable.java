package qrapp.school.mobile.mkotlarz.com.qrapp.fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.json.JSONObject;

import qrapp.school.mobile.mkotlarz.com.qrapp.R;
import qrapp.school.mobile.mkotlarz.com.qrapp.UI.models.UIModelTimetable;
import qrapp.school.mobile.mkotlarz.com.qrapp.interfaces.AsyncResponse;
import qrapp.school.mobile.mkotlarz.com.qrapp.utils.NetQuery;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Timetable.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Timetable#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Timetable extends Fragment implements AsyncResponse {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM_ID = "ID";
    private static final String ARG_PARAM_NAME = "NAME";

    // TODO: Rename and change types of parameters
    private int m_id;
    private String m_name;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param id id of timetable.
     * @param name name of timetable.
     * @return A new instance of fragment Timetable.
     */
    // TODO: Rename and change types and number of parameters
    public static Timetable newInstance(int id, String name) {
        Timetable fragment = new Timetable();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM_ID, id);
        args.putString(ARG_PARAM_NAME, name);
        fragment.setArguments(args);
        return fragment;
    }

    public Timetable() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            m_id = getArguments().getInt(ARG_PARAM_ID);
            m_name = getArguments().getString(ARG_PARAM_NAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        NetQuery query = new NetQuery();
        query.Delegate = this;
        query.execute("http://kmaciek.unixstorm.org/mobile_api/timetable/1/" + m_id + "/json");
        return inflater.inflate(R.layout.fragment_timetable, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public void PreExecute() {
        try {
            getActivity().findViewById(R.id.progressBar2).setVisibility(View.VISIBLE);

        } catch (Exception ex) {
                ex.printStackTrace();
        }
    }

    @Override
    public void PostExecute(JSONObject jsonObject) {
        try {
            getActivity().findViewById(R.id.progressBar2).setVisibility(View.GONE);
            UIModelTimetable timetable = new UIModelTimetable(jsonObject);
            timetable.Proceed();
            Toast.makeText(getActivity().getApplicationContext(), jsonObject.toString(), Toast.LENGTH_LONG).show();
        } catch(Exception ex) {
            Toast.makeText(getActivity().getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}
