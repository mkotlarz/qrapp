package qrapp.school.mobile.mkotlarz.com.qrapp.UI.models.Lessons;

/**
 * Created by Maciej Kotlarz on 2014-12-10.
 */
public class Lesson {

    public String Subject = null;
    public String Room = null;
    public String Teacher = null;
    public int Day = 0;
    private int m_subjectId = 0;
    private int m_roomId = 0;
    private int m_teacherId = 0;
    private int m_lessonId = 0;

    public Lesson(String Subject, String Room, String Teacher, int subjectId, int roomId, int teacherId, int day, int lessonId) {
        this.Subject = Subject;
        this.Room = Room;
        this.Teacher = Teacher;
        m_subjectId = subjectId;
        m_roomId = roomId;
        m_teacherId = teacherId;
        m_lessonId = lessonId;
        Day = day;
    }

    public Lesson() {}

    public int getSubjectId() { return m_subjectId; }
    public int getRoomId() { return m_roomId; }
    public int getTeacherId() { return m_teacherId; }
    public int getLessonId() { return m_lessonId; }
}
