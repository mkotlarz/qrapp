package qrapp.school.mobile.mkotlarz.com.qrapp.UI.models;

/**
 * Created by Maciej Kotlarz on 2014-12-08.
 */
public class SimpleSpinnerItem {
    private String m_name = null;
    private int m_ID = 0;

    public SimpleSpinnerItem(String name, int ID) {
        m_name = name;
        m_ID = ID;
    }

    public String getName() {
        return m_name;
    }

    public int getID() {
        return m_ID;
    }
}